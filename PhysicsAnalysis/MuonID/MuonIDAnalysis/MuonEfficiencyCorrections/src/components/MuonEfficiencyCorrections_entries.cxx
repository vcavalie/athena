#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"

#include "../MuonEfficiencyCorrections_TestAlg.h"
#include "../MuonEfficiencyCorrectionsProvider.h"
#include "../TestTrigSF.h"

DECLARE_COMPONENT( CP::MuonEfficiencyScaleFactors )

DECLARE_COMPONENT( CP::MuonTriggerScaleFactors )

DECLARE_COMPONENT( CP::MuonEfficiencyCorrections_TestAlg )

DECLARE_COMPONENT( CP::MuonEfficiencyCorrectionsProvider )

DECLARE_COMPONENT( Trig::TestTrigSF )

