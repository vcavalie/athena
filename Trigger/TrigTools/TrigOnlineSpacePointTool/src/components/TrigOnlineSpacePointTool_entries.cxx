
#include "../PixelSpacePointTool.h"
#include "../SCT_SpacePointTool.h"
#include "../PixelClusterCacheTool.h"
#include "../SCT_ClusterCacheTool.h"
#include "../OnlineSpacePointProviderTool.h"
#include "../FastSCT_RodDecoder.h"
#include "../TrigL2LayerNumberTool.h"
#include "../TrigSpacePointConversionTool.h"

DECLARE_COMPONENT( PixelSpacePointTool )
DECLARE_COMPONENT( SCT_SpacePointTool )
DECLARE_COMPONENT( PixelClusterCacheTool )
DECLARE_COMPONENT( SCT_ClusterCacheTool )
DECLARE_COMPONENT( FastSCT_RodDecoder )
DECLARE_COMPONENT( OnlineSpacePointProviderTool )
DECLARE_COMPONENT( TrigL2LayerNumberTool )
DECLARE_COMPONENT( TrigSpacePointConversionTool )

