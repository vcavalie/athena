#include "../egammaIso.h"
#include "../egammaShowerShape.h"
#include "../egammaEnergyPositionAllSamples.h"
#include "../egammaPreSamplerShape.h"
#include "../egammaStripsShape.h"
#include "../egammaMiddleShape.h"
#include "../egammaBackShape.h"
#include "../egammaqweta1c.h"
#include "../egammaqweta2c.h"
#include "../egammaCheckEnergyDepositTool.h"
#include "../egammaCaloClusterSelector.h"


DECLARE_COMPONENT( egammaShowerShape )
DECLARE_COMPONENT( egammaEnergyPositionAllSamples )
DECLARE_COMPONENT( egammaPreSamplerShape )
DECLARE_COMPONENT( egammaStripsShape )
DECLARE_COMPONENT( egammaMiddleShape )
DECLARE_COMPONENT( egammaBackShape )
DECLARE_COMPONENT( egammaIso )
DECLARE_COMPONENT( egammaqweta1c )
DECLARE_COMPONENT( egammaqweta2c )
DECLARE_COMPONENT( egammaCheckEnergyDepositTool )
DECLARE_COMPONENT( egammaCaloClusterSelector )
