// #include "tauRec/TauBuilder.h"
// #include "tauRec/TauProcessor.h"
#include "tauRec/TauProcessorAlg.h"
#include "tauRec/TauRunnerAlg.h"
#include "tauRec/TauTrackSlimmer.h"


// DECLARE_COMPONENT( TauBuilder )
// DECLARE_COMPONENT( TauProcessor )
DECLARE_COMPONENT( TauProcessorAlg )
DECLARE_COMPONENT( TauRunnerAlg )
DECLARE_COMPONENT( TauTrackSlimmer )


